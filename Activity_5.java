package hrm_project;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_5 {
	WebDriver driver;
	  @Test
	  public void editinfo() throws InterruptedException {
		  driver.findElement(By.id("txtUsername")).sendKeys("orange");
		  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		  driver.findElement(By.id("btnLogin")).click();
		  
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewMyDetails']")).click();
		 
		  driver.findElement(By.id("btnSave")).click();
		  driver.findElement(By.id("personal_txtEmpFirstName")).clear();
		  driver.findElement(By.id("personal_txtEmpFirstName")).sendKeys("Test1");
		  driver.findElement(By.id("personal_optGender_1")).click();
		  Select select = new Select(driver.findElement(By.id("personal_cmbNation")));
		  select.selectByVisibleText("Indian");
		  driver.findElement(By.id("personal_DOB")).clear();
		  driver.findElement(By.id("personal_DOB")).sendKeys("1990-09-18");
		  driver.findElement(By.id("btnSave")).click();
	  }
	  @BeforeClass
	  public void beforeClass() {
		  driver=new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/orangehrm");
		  
	  }

	  @AfterClass
	  public void afterClass() {
		  driver.close();
	  }
}
