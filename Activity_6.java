package hrm_project;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Activity_6 {
	WebDriver driver;
	  @Test
	  public void searchdir() throws InterruptedException {
		  driver.findElement(By.id("txtUsername")).sendKeys("orange");
		  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		  driver.findElement(By.id("btnLogin")).click();
		  
		  Thread.sleep(2000);
		  WebElement w=driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_directory_viewDirectory']"));
		  if(w.isDisplayed()==true);
		  {
			  w.click();
			 String heading = driver.findElement(By.xpath("//div[@class='head']")).getText();
			 System.out.println(heading);
			 Assert.assertEquals("Search Directory", heading);
		  }
		  
		 
	  }
	  @BeforeClass
	  public void beforeClass() {
		  driver=new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/orangehrm");
		  
	  }

	  @AfterClass
	  public void afterClass() {
		  driver.close();
	  }
}
