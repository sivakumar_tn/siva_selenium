package hrm_project;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;

public class Activity_10 {
	WebDriver driver;

	@Test
	public void login() throws InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Vacancies']")).click();
		driver.findElement(By.id("btnAdd")).click();
		
		Select select = new Select(driver.findElement(By.id("addJobVacancy_jobTitle")));
		select.selectByVisibleText("DevOps Engineer");;
		String vac="TestVac5";
		driver.findElement(By.id("addJobVacancy_name")).sendKeys(vac);
		String hiring_mgr = "siva tn";
		driver.findElement(By.id("addJobVacancy_hiringManager")).sendKeys(hiring_mgr);
		driver.findElement(By.id("addJobVacancy_noOfPositions")).sendKeys("3");
		driver.findElement(By.id("addJobVacancy_description")).sendKeys("TestVac");
		driver.findElement(By.id("btnSave")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Vacancies']")).click();
		Select s1=new Select(driver.findElement(By.id("vacancySearch_jobTitle")));
		s1.selectByVisibleText("DevOps Engineer");
		Select s2=new Select(driver.findElement(By.id("vacancySearch_hiringManager")));
		s2.selectByVisibleText(hiring_mgr);
		Select s3=new Select(driver.findElement(By.id("vacancySearch_jobVacancy")));
		s3.selectByVisibleText(vac);
		
		driver.findElement(By.id("btnSrch")).click();
		List<WebElement> list = driver.findElements(By.xpath("//table[@id='resultTable']/tbody/tr"));
		System.out.println(list.size());
		for (WebElement li : list) {
			System.out.println(li.getText());			
		}
		 
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
