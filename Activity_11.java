package hrm_project;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;

public class Activity_11 {
	WebDriver driver;

	@Test
	public void login() throws InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		//Thread.sleep(2000);
		driver.findElement(By.id("btnAdd")).click();
		driver.findElement(By.id("addCandidate_firstName")).sendKeys("sivakumar");
		driver.findElement(By.id("addCandidate_lastName")).sendKeys("tn");
		driver.findElement(By.id("addCandidate_email")).sendKeys("sivatn1990@gmail.com");
		driver.findElement(By.id("addCandidate_contactNo")).sendKeys("7418479965");
		Select select = new Select(driver.findElement(By.id("addCandidate_vacancy")));
		select.selectByVisibleText("TestVac");
		driver.findElement(By.id("addCandidate_resume")).sendKeys("C:\\Users\\SivakumarTN\\eclipse-workspace\\Project_HRM\\src\\Xyz.docx");
		driver.findElement(By.id("addCandidate_consentToKeepData")).click();
		
		driver.findElement(By.id("btnSave")).click();
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		
		Select select1 = new Select(driver.findElement(By.id("candidateSearch_hiringManager")));
		select1.selectByVisibleText("siva tn");
		driver.findElement(By.id("candidateSearch_candidateName")).sendKeys("sivakumar tn");
		driver.findElement(By.id("btnSrch")).click();
		System.out.println(driver.findElement(By.xpath("//table[@id='resultTable']/tbody/tr[1]")).getText());
		
		
		
		}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
