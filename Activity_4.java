package hrm_project;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class Activity_4 {
	WebDriver driver;
	WebDriverWait wait;

	@Test
	public void addEmlpoyee() throws InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		// wait=new WebDriverWait(driver, 10);
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewPimModule']"))));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewPimModule']")).click();
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[text()='AddEmployee']"))));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Add Employee']")).click();
		driver.findElement(By.id("firstName")).sendKeys("siva");
		driver.findElement(By.id("lastName")).sendKeys("tn");
		driver.findElement(By.id("chkLogin")).click();
		String username = "siva_tn5";
		driver.findElement(By.id("user_name")).sendKeys(username);
		driver.findElement(By.id("user_password")).sendKeys("Sivatn6666");
		driver.findElement(By.id("re_password")).sendKeys("Sivatn6666");
		driver.findElement(By.id("btnSave")).click();
		driver.findElement(By.xpath("//*[text()='Admin']")).click();
		driver.findElement(By.id("searchSystemUser_userName")).sendKeys(username);
		driver.findElement(By.id("searchBtn")).click();
		System.out.println(driver.findElement(By.xpath("//*[@id='resultTable']/tbody/tr/td/a")).getText());

	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
