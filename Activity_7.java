package hrm_project;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_7 {
	WebDriver driver;
	  @Test
	  public void qualification() throws InterruptedException {
		  driver.findElement(By.id("txtUsername")).sendKeys("orange");
		  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		  driver.findElement(By.id("btnLogin")).click();
		  
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewMyDetails']")).click();
		 
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		 
		  driver.findElement(By.xpath("//*[@id='sidebar']/ul/li/a[text()='Qualifications']")).click();
		  driver.findElement(By.id("addWorkExperience")).click();
		  
		  driver.findElement(By.id("experience_employer")).sendKeys("IBM");
		  driver.findElement(By.id("experience_jobtitle")).sendKeys("QA");
		  driver.findElement(By.id("experience_from_date")).clear();
		  driver.findElement(By.id("experience_from_date")).sendKeys("2016-03-02");
		  driver.findElement(By.id("experience_to_date")).clear();
		  driver.findElement(By.id("experience_to_date")).sendKeys("2024-03-02");
		  driver.findElement(By.id("experience_comments")).sendKeys("test data");
		  driver.findElement(By.id("btnWorkExpSave")).click();
		  
		  	 
		
	  }
	  @BeforeClass
	  public void beforeClass() {
		  driver=new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/orangehrm");
		  
	  }

	  @AfterClass
	  public void afterClass() {
		  driver.close();
	  }
}
