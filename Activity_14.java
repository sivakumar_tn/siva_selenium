package hrm_project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_14 {

	WebDriver driver;

	@Test
	public void readExcel() throws InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_recruitment_viewRecruitmentModule']")).click();
		driver.findElement(By.xpath("//*[text()='Vacancies']")).click();
		driver.findElement(By.id("btnAdd")).click();

		try {

			FileInputStream file = new FileInputStream("C:\\Users\\SivakumarTN\\eclipse-workspace\\Project_HRM\\src\\data vac.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			XSSFRow row = null;
			row = sheet.getRow(0);
			int rowCount = sheet.getLastRowNum() + 1;
			System.out.println("Row Count :- " + rowCount);

			for (int i = 0; i < rowCount; i++) {
				row = sheet.getRow(i);
				Select jobtitle = new Select(driver.findElement(By.id("addJobVacancy_jobTitle")));
				jobtitle.selectByVisibleText(row.getCell(0).getStringCellValue());
				String vac = row.getCell(1).getStringCellValue();
				driver.findElement(By.id("addJobVacancy_name")).sendKeys(vac);
				driver.findElement(By.id("addJobVacancy_hiringManager")).sendKeys(row.getCell(2).getStringCellValue());
				driver.findElement(By.id("btnSave")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_recruitment_viewRecruitmentModule']")).click();
				driver.findElement(By.xpath("//*[text()='Vacancies']")).click();

				Select jobtinput = new Select(driver.findElement(By.id("vacancySearch_jobVacancy")));
				Thread.sleep(2000);
				try
				{
				jobtinput.selectByVisibleText(vac);
				System.out.println("Vacancy created " +jobtinput.getFirstSelectedOption().getText());
				}
				catch (Exception e)
				{
					System.out.println("vacancy not added");
				}

				
				driver.findElement(By.id("btnAdd")).click();
				
				
				
			}

			file.close();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}
}