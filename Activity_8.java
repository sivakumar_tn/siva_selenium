package hrm_project;



import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_8 {
	WebDriver driver;
	  @Test
	  public void leave() throws InterruptedException {
		  driver.findElement(By.id("txtUsername")).sendKeys("orange");
		  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		  driver.findElement(By.id("btnLogin")).click();
		  //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_leave_viewLeaveModule']")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[text()='Apply']")).click();
		  Select select = new Select(driver.findElement(By.id("applyleave_txtLeaveType")));
		  select.selectByVisibleText("Paid Leave");
		  driver.findElement(By.id("applyleave_txtFromDate")).clear();
		  driver.findElement(By.id("applyleave_txtFromDate")).sendKeys("2020-03-27");
		  driver.findElement(By.id("applyleave_txtToDate")).clear();
		  driver.findElement(By.id("applyleave_txtToDate")).sendKeys("2020-03-27");
		  driver.findElement(By.id("applyleave_txtComment")).sendKeys("casual leave");
		  driver.findElement(By.id("applyBtn")).click();
		  driver.findElement(By.xpath("//*[text()='My Leave']")).click();
		  driver.findElement(By.id("calFromDate")).clear();
		  driver.findElement(By.id("calFromDate")).sendKeys("2020-04-01");
		  driver.findElement(By.id("calToDate")).clear();
		  driver.findElement(By.id("calToDate")).sendKeys("2020-04-01");
		  driver.findElement(By.id("btnSearch")).click();
		
	  }
	  @BeforeClass
	  public void beforeClass() {
		  driver=new FirefoxDriver();
		  driver.get("http://alchemy.hguy.co/orangehrm");
		  
	  }

	  @AfterClass
	  public void afterClass() {
		  //driver.close();
	  }
}
