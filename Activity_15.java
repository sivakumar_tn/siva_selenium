package hrm_project;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity_15 {
	WebDriver driver;
    
	@Test(priority=0)
	 public void testCase1() {
	
        String title = driver.getTitle();
	    Reporter.log("Title is: " + title);
	    Assert.assertEquals("OrangeHRM", title);
	
    }

	@Test
	public void login() throws InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewMyDetails']")).click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		driver.findElement(By.xpath("//*[@id='sidebar']/ul/li/a[text()='Emergency Contacts']")).click();
		//emgcontact_list
		List<WebElement> list=driver.findElements(By.xpath("//table[@id='emgcontact_list']/tbody/tr"));
		System.out.println(list.size());
		for (WebElement li : list) {
			System.out.println(li.getText());
		}
		 Reporter.log("Contacts Found");
		 
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");
		 Reporter.log("Test Started");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
		  Reporter.log("Test Completed");
	}

}
