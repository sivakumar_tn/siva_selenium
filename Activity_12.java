package hrm_project;

import org.testng.annotations.Test;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.testng.annotations.BeforeClass;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity_12 {
	WebDriver driver;

	@Test
	public void login() throws IOException, CsvException, InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		Thread.sleep(2000);
 		driver.findElement(By.xpath("//div[@class='menu']/ul/li/a[@id='menu_pim_viewPimModule']")).click();
 		Thread.sleep(2000);
 		driver.findElement(By.xpath("//*[text()='Add Employee']")).click();
 		
		
		 CSVReader reader = new CSVReader(new FileReader("C:\\Users\\SivakumarTN\\eclipse-workspace\\Project_HRM\\src\\data1.csv"));

	        //Load content into list
		
		  String [] csvCell;
		  //while loop will be executed till the last line In CSV.
		  while ((csvCell = reader.readNext()) != null) {   
		   String FName = csvCell[0];
		   String LName = csvCell[1];
		   String UName = csvCell[2];
		   String Pass = csvCell[3];
		  
		   
		  System.out.println(FName);
		  System.out.println(LName);
		  driver.findElement(By.id("firstName")).clear();
		  driver.findElement(By.id("firstName")).sendKeys(FName);
		  driver.findElement(By.id("lastName")).clear();
		  driver.findElement(By.id("lastName")).sendKeys(LName);
		  driver.findElement(By.id("chkLogin")).click();
	 	  try
		  {
			  driver.findElement(By.id("user_name")).clear();
			  
		  }
		  catch (ElementNotInteractableException e)
		  {
			  driver.findElement(By.id("chkLogin")).click();
			  Thread.sleep(2000);
			  driver.findElement(By.id("user_name")).clear();
			  
		  }
		  
		  driver.findElement(By.id("user_name")).sendKeys(UName);
		  driver.findElement(By.id("user_password")).clear();
		  driver.findElement(By.id("user_password")).sendKeys(Pass);
		  driver.findElement(By.id("re_password")).clear();
		  driver.findElement(By.id("re_password")).sendKeys(Pass);
		  String uid = driver.findElement(By.id("employeeId")).getText();
		  driver.findElement(By.id("btnSave")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[text()='Employee List']")).click();
		  driver.findElement(By.id("empsearch_id")).sendKeys(uid);
		  driver.findElement(By.id("searchBtn")).click();
		  System.out.println(driver.findElement(By.xpath("//table[@id='resultTable']/tbody/tr")).getText());
		  
		  driver.findElement(By.xpath("//*[text()='Add Employee']")).click();
	 	          
	        }
	        
	        reader.close();
			}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
