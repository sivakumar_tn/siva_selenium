package hrm_project;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity_3 {
	WebDriver driver;

	@Test
	public void login() {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals("http://alchemy.hguy.co:8080/orangehrm/symfony/web/index.php/dashboard", driver.getCurrentUrl());
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
