package hrm_project;

import org.testng.annotations.Test;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.testng.annotations.BeforeClass;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity_13 {
	WebDriver driver;

	@Test
	public void login() throws CsvValidationException, IOException, InterruptedException {
		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();
		driver.navigate().to("http://alchemy.hguy.co:8080/orangehrm/symfony/web/index.php/admin/pimCsvImport");
		driver.findElement(By.id("pimCsvImport_csvFile")).sendKeys("C:\\Users\\SivakumarTN\\eclipse-workspace\\Project_HRM\\src\\importData1.csv");
		driver.findElement(By.id("btnSave")).click();
		driver.findElement(By.xpath("//*[text()='Employee List']")).click();
		CSVReader reader = new CSVReader(new FileReader("C:\\\\Users\\\\SivakumarTN\\\\eclipse-workspace\\\\Project_HRM\\\\src\\\\importData1.csv"));
		String[] csvCell;
		while ((csvCell = reader.readNext()) != null) {

			String empid = csvCell[3];

			System.out.println(empid);
			driver.findElement(By.id("empsearch_id")).clear();
			driver.findElement(By.id("empsearch_id")).sendKeys(empid);
			driver.findElement(By.id("searchBtn")).click();

			if (driver.findElement(By.xpath("//table[@id='resultTable']/tbody/tr")).getText().contains(empid)) {
				System.out.println("record added");
			} else {
				System.out.println("no record");
			}

		}

		reader.close();
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://alchemy.hguy.co/orangehrm");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
